import {Injectable} from '@angular/core';
import {Subject, BehaviorSubject, Observable} from 'rxjs';
import {Http, URLSearchParams} from '@angular/http';
import { AppConfig } from './app.config';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';
// http request module
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private API_URL = AppConfig.API_URL;

  loggedIn: Subject<boolean> = new BehaviorSubject<boolean>(false); // 用户登陆状态

  constructor(private http: Http, private router: Router) { }

  login(user): Observable<any> {
    const params = new URLSearchParams();
    params.set('username', user.username);
    params.set('password', user.password);
    return this.http.post(this.API_URL + '/login', params, {withCredentials: true})
      .pipe(map((res) => {
        this.loggedIn.next(res.json().success);
        if (this.loggedIn){
          this.router.navigate(['/lc']);  // 登录成功后，跳到lc路由
        }
        return res;
      }));
  }

  checklogin(): Observable<any> {
    return this.http.get(this.API_URL + '/checklogin', {withCredentials: true})
      .pipe(map((res) => {
        this.loggedIn.next(res.json().success);
        return res;
      }));
  }

  logout(): Observable<any> {
    return this.http.post(this.API_URL + '/logout', {}, {withCredentials: true})
      .pipe(map(res => {
        res.json();
        this.loggedIn.next(false);
        this.router.navigate(['/login']);
        return res;
      }));
  }

  register(user): Observable<any> {
    return this.http.post(this.API_URL + "/users", user)
      .pipe(map(res => {
        res.json();
        this.router.navigate(['/login']);
      }));
  }
}
